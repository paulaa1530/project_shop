package com.sda;

import com.sda.ee.mvc.controllers.AuctionController;
import com.sda.ee.mvc.controllers.CommentController;
import com.sda.ee.mvc.controllers.ProductController;
import com.sda.ee.mvc.controllers.UserController;
import com.sda.ee.mvc.model.State;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        UserController userController = new UserController();
        ProductController productController = new ProductController();
        AuctionController auctionController = new AuctionController();
        CommentController commentController = new CommentController();

        Integer currentUserId = null;

        Scanner sc = new Scanner(System.in);

        boolean isWorking = true;
        while (isWorking) {
            String line = sc.nextLine().toLowerCase();

            if (line.startsWith("register")) {
                String[] splits = line.split(" ");

                String newLogin = splits[1];
                String newPassword = splits[2];

                String hash = MD5(newPassword);
                System.out.println(hash);

                String newEmail = splits[3];

                userController.registerUser(newLogin, hash, newEmail);
            } else if (line.startsWith("login")) {
                if (currentUserId == null) {
                    String[] splits = line.split(" ");

                    String newLogin = splits[1];
                    String newPassword = splits[2];

                    String hash = MD5(newPassword);

                    currentUserId = userController.loginUser(newLogin, hash);
                } else {
                    System.out.println("Unable to login, log out first!");
                }
            } else if (line.equalsIgnoreCase("logout")) {
                if (currentUserId == null) {
                    System.out.println("Already logged out");
                } else {
                    currentUserId = null;
                }
            }else if (line.startsWith("addproduct")){
                if (currentUserId == null){
                    System.out.println("Jesteś niezalogowany - nie możesz dodać produktu");
                }else {
                    String[] splits = line.split(" ");

                    String name = splits[1];
                    String state = splits[2];

                    productController.addProduct(name, State.valueOf(state.toUpperCase()));
                }
            }else if (line.startsWith("listproducts")){
                productController.listAllProducts();
            }else if (line.startsWith("addauction")) {
                if (currentUserId == null) {
                    System.out.println("To add new auction please login first");
                } else {
                    String[] splits = line.split(" ");

                    String title = splits[1];
                    String price = splits[2];
                    String amount = splits[3];
                    String productId = splits[4];


                    auctionController.addAuction(currentUserId, title, Double.parseDouble(price),
                            Integer.parseInt(amount), Integer.parseInt(productId));
                }
            }else if (line.startsWith("listauctions")){
                auctionController.listAllAuctions();
            }else if (line.startsWith("addcomment")){
                if (currentUserId==null){
                    System.out.println("To comment please login");
                }else {
                    String[] splits = line.split(" ");

                    String tekst = splits[1];
                    String auctionId = splits[2];

                    commentController.addComment(currentUserId, tekst, Integer.parseInt(auctionId));
                }
            }
        }
    }

    public static String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }
}
