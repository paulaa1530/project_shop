package com.sda.ee.mvc;

import com.sda.ee.mvc.dao.*;
import com.sda.ee.mvc.services.*;

public class BeanController {
    private static final IUserService userServiceImpl = new UserServiceImpl();
    private static IUserDao userDaoImpl = new UserDao();
    private static IAuctionService auctionService = new AuctionServiceImpl();
    private static IProductService productService = new ProductServiceImpl();
    private static IAuctionDao auctionDao = new AuctionDao();
    private static IProductDao productDao = new ProductDao();
    private static ICommentService commentService = new CommentServiceImpl();
    private static ICommentDao commentDao = new CommentDao();


    // getter user service
    public static IUserService getUserServiceImpl() {
        return userServiceImpl;
    }

    // getter user dao
    public static IUserDao getUserDaoImpl() {
        if(userDaoImpl == null){
            userDaoImpl = new UserDao();
        }

        return userDaoImpl;
    }

    public static IAuctionDao getAuctionDao() {
        if (auctionDao == null){
            auctionDao = new AuctionDao();
        }
        return auctionDao;
    }

    public static IAuctionService getAuctionService() {
        return auctionService;
    }

    public static IProductService getProductService() {
        return productService;
    }

    public static IProductDao getProductDao() {
        if (productDao ==null){
            productDao=new ProductDao();
        }
        return productDao;
    }

    public static ICommentDao getCommentDao() {
        if (commentDao ==null){
            commentDao = new CommentDao();
        }
        return commentDao;
    }

    public static ICommentService getCommentService() {
        return commentService;
    }
}
