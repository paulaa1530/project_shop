package com.sda.ee.mvc.services;


import com.sda.ee.mvc.BeanController;
import com.sda.ee.mvc.dao.ICommentDao;
import com.sda.ee.mvc.model.Comment;

public class CommentServiceImpl implements ICommentService {
    private static int NEXT_COMMENT_ID = 0;

    private ICommentDao commentDao = BeanController.getCommentDao();

    @Override
    public boolean addComment(int userId, String tekst, int auctionId) {
        Comment toAdd = new Comment(NEXT_COMMENT_ID++, tekst, auctionId, userId);

        commentDao.addComment(toAdd);

        return false;

    }


}
