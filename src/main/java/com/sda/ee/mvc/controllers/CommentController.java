package com.sda.ee.mvc.controllers;

import com.sda.ee.mvc.BeanController;
import com.sda.ee.mvc.exceptions.ItemNotFoundException;
import com.sda.ee.mvc.model.Auction;
import com.sda.ee.mvc.services.IAuctionService;
import com.sda.ee.mvc.services.ICommentService;
import com.sda.ee.mvc.services.IProductService;

public class CommentController {
    private ICommentService commentService = BeanController.getCommentService();
    private IAuctionService auctionService = BeanController.getAuctionService();


    public void addComment(int userId, String tekst, int auctionId) {
        Auction auction = null;
        try{
            auction = auctionService.getAuctionWithId(auctionId);
        } catch (ItemNotFoundException e) {
            System.out.println("This auction does not exist"+e.getMessage());
            return;
        }
        if (auction !=  null){
            commentService.addComment(userId, tekst, auctionId);
            System.out.println("Comment added!");
        }else {
            System.out.println("This auction does not exist");
        }
    }

}
