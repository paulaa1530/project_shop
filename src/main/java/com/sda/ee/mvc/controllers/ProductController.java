package com.sda.ee.mvc.controllers;

import com.sda.ee.mvc.BeanController;
import com.sda.ee.mvc.model.Product;
import com.sda.ee.mvc.model.State;
import com.sda.ee.mvc.services.IProductService;

import java.util.List;

public class ProductController {

    private IProductService productService = BeanController.getProductService();



    public void addProduct(String name, State state){
        if(productService.addProduct(name, state)){
            System.out.println("Product added! ");
        }else {
            System.out.println("Error adding product");
        }
    }

    public void listAllProducts(){
        List<Product> productList = productService.getAllProducts();
        productList.stream().forEach(System.out::println);
    }
}
