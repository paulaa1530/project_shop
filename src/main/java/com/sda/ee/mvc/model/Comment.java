package com.sda.ee.mvc.model;

public class Comment {
    private int id;
    private String tekst;
    private int auctionId;
    private  int commenterId;

    public Comment(int id, String tekst, int auctionId, int commenterId) {
        this.id = id;
        this.tekst = tekst;
        this.auctionId = auctionId;
        this.commenterId = commenterId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTekst() {
        return tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    public int getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(int auctionId) {
        this.auctionId = auctionId;
    }

    public int getCommenterId() {
        return commenterId;
    }

    public void setCommenterId(int commenterId) {
        this.commenterId = commenterId;
    }
}
