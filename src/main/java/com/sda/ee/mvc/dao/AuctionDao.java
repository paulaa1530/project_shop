package com.sda.ee.mvc.dao;

import com.sda.ee.mvc.model.Auction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class AuctionDao implements IAuctionDao{

    public Map<Integer, Auction> auctions = new HashMap<>();

    @Override
    public void addAuction(Auction toAdd) {
        auctions.put(toAdd.getId(), toAdd);
    }

    @Override
    public List<Auction> getAllAuctions() {
        return auctions.values().stream().collect(Collectors.toList());
    }

    @Override
    public Optional<Auction> getAuctionWithId(int auctionId) {
        return Optional.ofNullable(auctions.get(auctionId));
    }
}
