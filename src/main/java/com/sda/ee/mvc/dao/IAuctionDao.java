package com.sda.ee.mvc.dao;

import com.sda.ee.mvc.model.Auction;

import java.util.List;
import java.util.Optional;

public interface IAuctionDao {
    void addAuction(Auction toAdd);

    List<Auction> getAllAuctions();

    Optional<Auction> getAuctionWithId(int auctionId);
}
