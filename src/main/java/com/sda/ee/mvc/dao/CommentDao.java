package com.sda.ee.mvc.dao;

import com.sda.ee.mvc.model.Comment;

import java.util.HashMap;
import java.util.Map;

public class CommentDao implements ICommentDao {
    public Map<Integer, Comment> comments = new HashMap<>();

    @Override
    public void addComment(Comment toAdd) {
        comments.put(toAdd.getAuctionId(), toAdd);
    }
}
