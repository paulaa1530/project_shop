package com.sda.ee.mvc.dao;

import com.sda.ee.mvc.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserDao implements IUserDao {

    // data access object
    // obiekt który dostarcza danych nt. innych obiektów z baz danych itd.
    private List<User> list = new ArrayList<User>();

    public List<User> getAllUsers() {
        return list;
    }

    @Override
    public boolean checkIfUserOrEmailExists(String newLogin, String newEmail) {
        Optional<User> optionalUser = list.stream()
                .filter(u -> u.getLogin().equalsIgnoreCase(newLogin))
                .filter(u -> u.getEmail().equalsIgnoreCase(newEmail))
                .findAny();

        // jeśli taki user nie istnieje to zwracamy true!
        // sukces jest wtedy, kiedy taki użytkownik nie istnieje
        return !optionalUser.isPresent();
    }

    @Override
    public void addUser(User user) {
        list.add(user);
    }
}
