package com.sda.ee.mvc.dao;

import com.sda.ee.mvc.model.Comment;

public interface ICommentDao {
    void addComment(Comment toAdd);
}
